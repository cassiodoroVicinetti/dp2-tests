package it.polito.dp2.FDS.lab2.tests.ghioluca;

import static org.junit.Assert.*;

import java.io.File;
import java.util.*;

import org.junit.*;

import it.polito.dp2.FDS.*;
import it.polito.dp2.FDS.lab2.tests.*;
import it.polito.dp2.FDS.tests.LoggerManager;

public class Tests extends TestsAbstract {
	private final static LoggerManager logger = new LoggerManager(Tests.class.getName());
	private final static String parentFolder = "ghioluca";
	
	/**
	 * The seat is to be unique within a flight instance
	 * @throws FactoryConfigurationError 
	 * @throws Exception 
	 */
	@Test(expected = FlightMonitorException.class)
	public void test1() throws FactoryConfigurationError, Exception {
		final int testId = 1;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), getSimpleFlightMonitorFactoryClass(testId));
		
		runParser(testFolder, testFile.toString());
	}
	
	/**
	 * Simple test on start date
	 * @throws FactoryConfigurationError
	 * @throws Exception 
	 */
	@Test
	public void test2() throws FactoryConfigurationError, Exception {
		final int testId = 2;
		final int testCase = 2;
		final int seed = 12343;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");

		FlightMonitor refFlightMonitor = runRandomGenerator(testCase, seed);
		final int refFlightInstancesSize = refFlightMonitor.getFlightInstances("XP712", it.polito.dp2.FDS.tests.Common.parseDate("2010-02-28 GMT+01:00"), FlightInstanceStatus.BOOKING).size();
		
		logger.fine("Size of reference flight instance list = " + refFlightInstancesSize);
		
		runSerializer(testFile.toString(), testCase, seed);
		
		FlightMonitor testFlightMonitor = runParser(testFolder, testFile.toString());
		final int testFlightInstancesSize = testFlightMonitor.getFlightInstances("XP712", it.polito.dp2.FDS.tests.Common.parseDate("2010-02-28 GMT+01:00"), FlightInstanceStatus.BOOKING).size();
		logger.fine("Size of test flight instance list = " + refFlightInstancesSize);
		
		assertEquals("Size of flight instance list", refFlightInstancesSize, testFlightInstancesSize);
	}

	/**
	 * The start date is including and hour, minute, second, millisecond, time zone, etc. are not to be considered
	 * @throws FactoryConfigurationError
	 * @throws Exception 
	 */
	@Test
	public void test2_limit() throws FactoryConfigurationError, Exception {
		final int testId = 2;
		final int testCase = 2;
		final int seed = 12343;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");

		// WARNING: The reference FlightMonitor DOES consider hour, minute, second, millisecond, time zone, etc.
		/*FlightMonitor refFlightMonitor = runRandomGenerator(testCase, seed);
		final int refFlightInstancesSize = refFlightMonitor.getFlightInstances("XP712", it.polito.dp2.FDS.tests.Common.parseDate("2010-02-28 GMT-08:00"), FlightInstanceStatus.BOOKING).size();*/
		
		final int refFlightInstancesSize = 4;
		logger.fine("Size of reference flight instance list = " + refFlightInstancesSize);
		
		runSerializer(testFile.toString(), testCase, seed);
		
		FlightMonitor testFlightMonitor = runParser(testFolder, testFile.toString());
		final int testFlightInstancesSize = testFlightMonitor.getFlightInstances("XP712", it.polito.dp2.FDS.tests.Common.parseDate("2010-02-28 GMT-08:00"), FlightInstanceStatus.BOOKING).size();
		logger.fine("Size of test flight instance list = " + refFlightInstancesSize);
		
		assertEquals("Size of flight instance list", refFlightInstancesSize, testFlightInstancesSize);
	}
	
	/**
	 * The flight number is to be in the following format: 2 alphabetic characters followed by up to 4 digits
	 * @throws Exception
	 */
	@Test(expected = javax.xml.bind.MarshalException.class)
	public void test3() throws Exception {
		final int testId = 3;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), getSimpleFlightMonitorFactoryClass(testId));
	}
	
	/**
	 * The airport code is to be in the following format: 3 alphabetic characters
	 * @throws Exception
	 */
	@Test(expected = javax.xml.bind.MarshalException.class)
	public void test4() throws Exception {
		final int testId = 4;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), getSimpleFlightMonitorFactoryClass(testId));
	}
	
	/**
	 * The airport code is to be in the following format: 3 alphabetic characters
	 * @throws Exception
	 */
	@Test(expected = MalformedArgumentException.class)
	public void test5() throws Exception {
		final int testId = 5;
		final int testCase = 0;
		final int seed = 12343;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), testCase, seed);
		
		FlightMonitor testFlightMonitor = runParser(testFolder, testFile.toString());
		
		testFlightMonitor.getFlights("X1Z", "ABC", new Time(1, 1));
	}
	
	/**
	 * The flight number is to be in the following format: 2 alphabetic characters followed by up to 4 digits
	 * @throws Exception
	 */
	@Test(expected = MalformedArgumentException.class)
	public void test6() throws Exception {
		final int testId = 6;
		final int testCase = 0;
		final int seed = 12343;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), testCase, seed);
		
		FlightMonitor testFlightMonitor = runParser(testFolder, testFile.toString());
		
		testFlightMonitor.getFlightInstances("AB12345", null, null);
	}
	
	/**
	 * The flight number is to be in the following format: 2 alphabetic characters followed by up to 4 digits
	 * @throws Exception
	 */
	@Test(expected = MalformedArgumentException.class)
	public void test7() throws Exception {
		final int testId = 7;
		final int testCase = 0;
		final int seed = 12343;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), testCase, seed);
		
		FlightMonitor testFlightMonitor = runParser(testFolder, testFile.toString());
		
		testFlightMonitor.getFlight("AB12345");
	}
	
	/**
	 * The flight number is to be in the following format: 2 alphabetic characters followed by up to 4 digits
	 * @throws Exception
	 */
	@Test(expected = MalformedArgumentException.class)
	public void test8() throws Exception {
		final int testId = 8;
		final int testCase = 0;
		final int seed = 12343;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), testCase, seed);
		
		FlightMonitor testFlightMonitor = runParser(testFolder, testFile.toString());
		
		testFlightMonitor.getFlightInstance("AB12345", new GregorianCalendar());
	}
	
	/**
	 * When a passenger has already been boarded seat cannot be null
	 * @throws Exception
	 */
	@Test(expected = FlightMonitorException.class)
	public void test9() throws Exception {
		final int testId = 9;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), getSimpleFlightMonitorFactoryClass(testId));
		
		runParser(testFolder, testFile.toString());
	}

	/**
	 * The aircraft model is to be unique
	 * @throws Exception
	 */
	@Test(expected = javax.xml.bind.MarshalException.class)
	public void test10() throws Exception {
		final int testId = 10;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), getSimpleFlightMonitorFactoryClass(testId));
	}
	
	/**
	 * Airport codes could be null
	 * @throws Exception 
	 */
	@Test
	public void test11() throws Exception {
		final int testId = 11;
		final int testCase = 2;
		final int seed = 12343;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), testCase, seed);
		
		FlightMonitor refFlightMonitor = runRandomGenerator(testCase, seed);
		int refFlightsSize = refFlightMonitor.getFlights(null, "LTN", null).size();
		logger.fine("Size of reference flight list = " + refFlightsSize);
		
		FlightMonitor testFlightMonitor = runParser(testFolder, testFile.toString());
		int testFlightsSize = testFlightMonitor.getFlights(null, "LTN", null).size();
		logger.fine("Size of test flight list = " + testFlightsSize);
		
		assertEquals("Size of flights list", refFlightsSize, testFlightsSize);
	}

	/**
	 * The same seat could exist in two different aircrafts
	 * @throws Exception
	 */
	@Test
	public void test12() throws Exception {
		final int testId = 12;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), getSimpleFlightMonitorFactoryClass(testId));
		
		FlightMonitor testFlightMonitor = runParser(testFolder, testFile.toString());
		
		Set<Aircraft> aircrafts = testFlightMonitor.getAircrafts();
		assertEquals(2, aircrafts.size());
		
		Iterator<Aircraft> i = aircrafts.iterator();
		while (i.hasNext()) {
			Aircraft aircraft = i.next();
			assertEquals(1, aircraft.seats.size());
			assertTrue(aircraft.seats.iterator().next().equals("1A"));
		}
	}

	/**
	 * The flight is to be unique
	 * @throws Exception
	 */
	@Test(expected = javax.xml.bind.MarshalException.class)
	public void test13() throws Exception {
		final int testId = 13;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), getSimpleFlightMonitorFactoryClass(testId));
	}

	/**
	 * The flight instance is to be unique within the flight
	 * @throws Exception
	 */
	@Test(expected = FlightMonitorException.class)
	public void test14() throws Exception {
		final int testId = 14;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), getSimpleFlightMonitorFactoryClass(testId));
		
		runParser(testFolder, testFile.toString());
	}

	/**
	 * The flight instance is to be unique within the flight
	 * @throws Exception
	 */
	@Test(expected = FlightMonitorException.class)
	public void test14_limit() throws Exception {
		final int testId = 14;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), getSimpleFlightMonitorFactoryClass(testId, true));
		
		runParser(testFolder, testFile.toString());
	}

	/**
	 * The same flight instance could exist in two different flights
	 * @throws Exception
	 */
	@Test
	public void test15() throws Exception {
		final int testId = 15;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), getSimpleFlightMonitorFactoryClass(testId));
		
		FlightMonitor testFlightMonitor = runParser(testFolder, testFile.toString());
		
		List<FlightInstanceReader> flightInstances = testFlightMonitor.getFlightInstances(null, null, null);
		assertEquals(2, flightInstances.size());
	}

	/**
	 * The passenger is to be unique within the flight instance
	 * @throws Exception
	 */
	@Test(expected = javax.xml.bind.MarshalException.class)
	public void test16() throws Exception {
		final int testId = 16;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), getSimpleFlightMonitorFactoryClass(testId));
	}

	/**
	 * The same passenger could exist in two different flight instances
	 * @throws Exception
	 */
	@Test
	public void test17() throws Exception {
		final int testId = 17;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), getSimpleFlightMonitorFactoryClass(testId));
		
		FlightMonitor testFlightMonitor = runParser(testFolder, testFile.toString());
		
		List<FlightInstanceReader> flightInstances = testFlightMonitor.getFlightInstances(null, null, null);
		assertEquals(2, flightInstances.size());
		
		Iterator<FlightInstanceReader> i = flightInstances.iterator();
		while (i.hasNext()) {
			FlightInstanceReader flightInstance = i.next();
			Set<PassengerReader> passengers = flightInstance.getPassengerReaders(null);
			assertEquals(1, passengers.size());
			assertTrue(passengers.iterator().next().getName().equals("Pinco Pallino"));
		}
	}
	
	/**
	 * The flight could not exist
	 * @throws Exception
	 */
	@Test
	public void test18() throws Exception {
		final int testId = 18;
		final int testCase = 2;
		final int seed = 12343;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), testCase, seed);
		
		FlightMonitor testFlightMonitor = runParser(testFolder, testFile.toString());
		
		List<FlightInstanceReader> flightInstances = testFlightMonitor.getFlightInstances("AB123", null, null);
		assertNotNull(flightInstances);
		assertEquals(0, flightInstances.size());
	}

	/**
	 * The flight could not exist
	 * @throws Exception
	 */
	@Test
	public void test19() throws Exception {
		final int testId = 19;
		final int testCase = 2;
		final int seed = 12343;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), testCase, seed);
		
		FlightMonitor testFlightMonitor = runParser(testFolder, testFile.toString());
		
		FlightReader flight = testFlightMonitor.getFlight("AB123");
		assertNull(flight);
	}

	/**
	 * The flight could not exist
	 * @throws Exception
	 */
	@Test
	public void test20() throws Exception {
		final int testId = 20;
		final int testCase = 2;
		final int seed = 12343;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), testCase, seed);
		
		FlightMonitor testFlightMonitor = runParser(testFolder, testFile.toString());
		
		FlightInstanceReader flightInstance = testFlightMonitor.getFlightInstance("AB123", new GregorianCalendar(2015, 0, 24));
		assertNull(flightInstance);
	}

	/**
	 * The flight instance could not exist
	 * @throws Exception
	 */
	@Test
	public void test21() throws Exception {
		final int testId = 21;
		final int testCase = 2;
		final int seed = 12343;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), testCase, seed);
		
		FlightMonitor testFlightMonitor = runParser(testFolder, testFile.toString());
		
		FlightInstanceReader flightInstance = testFlightMonitor.getFlightInstance("ZX186", it.polito.dp2.FDS.tests.Common.parseDate("2010-04-07 CET"));
		assertNull(flightInstance);
	}
	
	/**
	 * The flight instance could not exist
	 * @throws Exception
	 */
	@Test
	public void test21_limit() throws Exception {
		final int testId = 21;
		final int testCase = 2;
		final int seed = 12343;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), testCase, seed);
		
		FlightMonitor testFlightMonitor = runParser(testFolder, testFile.toString());
		
		FlightInstanceReader flightInstance = testFlightMonitor.getFlightInstance("ZX186", it.polito.dp2.FDS.tests.Common.parseDate("2010-03-07 GMT+02:00"));
		assertNull(flightInstance);
	}
	
	/**
	 * The aircraft must exist
	 * @throws Exception
	 */
	@Test(expected = javax.xml.bind.MarshalException.class)
	public void test22() throws Exception {
		final int testId = 22;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), getSimpleFlightMonitorFactoryClass(testId));
	}
	
	/**
	 * The seat must exist in the aircraft
	 * @throws Exception
	 */
	@Test(expected = FlightMonitorException.class)
	public void test23() throws Exception {
		final int testId = 23;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), getSimpleFlightMonitorFactoryClass(testId));
		
		runParser(testFolder, testFile.toString());
	}
	
	/**
	 * Two flight instances with different time zones can not exist within the same flight
	 * @throws Exception
	 */
	@Test(expected = FlightMonitorException.class)
	public void test24() throws Exception {
		final int testId = 24;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), getSimpleFlightMonitorFactoryClass(testId));
		
		runParser(testFolder, testFile.toString());
	}
	
	/**
	 * The departure gate could be null
	 * @throws Exception
	 */
	@Test
	public void test25() throws Exception {
		final int testId = 25;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), getSimpleFlightMonitorFactoryClass(testId));
		
		FlightMonitor testFlightMonitor = runParser(testFolder, testFile.toString());
		
		List<FlightInstanceReader> flightInstances = testFlightMonitor.getFlightInstances("AB123", null, null);
		assertEquals(1, flightInstances.size());
		
		assertNull(flightInstances.iterator().next().getDepartureGate());
	}

	/**
	 * The seat could be null
	 * @throws Exception
	 */
	@Test
	public void test26() throws Exception {
		final int testId = 26;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), getSimpleFlightMonitorFactoryClass(testId));
		
		FlightMonitor testFlightMonitor = runParser(testFolder, testFile.toString());
		
		List<FlightInstanceReader> flightInstances = testFlightMonitor.getFlightInstances("AB123", null, null);
		assertEquals(1, flightInstances.size());
		
		Set<PassengerReader> passengers = flightInstances.iterator().next().getPassengerReaders(null);
		assertEquals(1, passengers.size());
		
		assertNull(passengers.iterator().next().getSeat());
	}
	
	/**
	 * Simple test on start time
	 * @throws Exception 
	 */
	@Test
	public void test27() throws Exception {
		final int testId = 27;
		final int testCase = 2;
		final int seed = 12343;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), testCase, seed);
		
		FlightMonitor refFlightMonitor = runRandomGenerator(testCase, seed);
		final int refFlightsSize = refFlightMonitor.getFlights("MPX", "LTN", new Time(13, 22)).size();
		
		logger.fine("Size of reference flight list = " + refFlightsSize);
		
		FlightMonitor testFlightMonitor = runParser(testFolder, testFile.toString());
		int testFlightsSize = testFlightMonitor.getFlights("MPX", "LTN", new Time(13, 22)).size();
		logger.fine("Size of test flight list = " + testFlightsSize);
		
		assertEquals("Size of flights list", refFlightsSize, testFlightsSize);
	}
	
	/**
	 * Start time is to be excluding
	 * @throws Exception 
	 */
	@Test
	public void test27_limit() throws Exception {
		final int testId = 27;
		final int testCase = 2;
		final int seed = 12343;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), testCase, seed);
		
		FlightMonitor refFlightMonitor = runRandomGenerator(testCase, seed);
		final int refFlightsSize = refFlightMonitor.getFlights("MPX", "LTN", new Time(13, 23)).size();
		logger.fine("Size of reference flight list = " + refFlightsSize);
		
		FlightMonitor testFlightMonitor = runParser(testFolder, testFile.toString());
		int testFlightsSize = testFlightMonitor.getFlights("MPX", "LTN", new Time(13, 23)).size();
		logger.fine("Size of test flight list = " + testFlightsSize);
		
		assertEquals("Size of flights list", refFlightsSize, testFlightsSize);
	}
	
	/**
	 * A flight instance with time zone "Europe/Rome" (during winter) is the same as a flight instance with time zone "GMT+01:00"
	 * @throws Exception
	 */
	@Test
	public void test28() throws Exception {
		final int testId = 28;
		
		File testFolder = getTestFolder(parentFolder, testId);
		File testFile = getTestFile(testFolder, "out.xml");
		
		runSerializer(testFile.toString(), getSimpleFlightMonitorFactoryClass(testId));
		
		FlightMonitor testFlightMonitor = runParser(testFolder, testFile.toString());
		
		FlightInstanceReader flightInstance = testFlightMonitor.getFlightInstance("AB123", it.polito.dp2.FDS.tests.Common.parseDate("2014-11-08 Europe/Rome"));
		assertNotNull(flightInstance);
	}
	
	private static String getSimpleFlightMonitorFactoryClass(int testId) {
		return getSimpleFlightMonitorFactoryClass(testId, false);
	}
	
	private static String getSimpleFlightMonitorFactoryClass(int testId, boolean limit) {
		return "it.polito.dp2.FDS.lab2.tests.ghioluca.test" + testId + ((limit) ? "_limit" : "") + ".SimpleFlightMonitorFactory";
	}
}