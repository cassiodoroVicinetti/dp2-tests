package it.polito.dp2.FDS.tests;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Common {
	// Parameters
	public static final Level minimumLoggingLevel = Level.INFO;
	
	public static void setDebugLevel(Level newLvl) {
		// http://stackoverflow.com/a/25677104/1267803
	    Logger anonymousLogger = LogManager.getLogManager().getLogger("");
	    Handler[] handlers = anonymousLogger.getHandlers();
	    anonymousLogger.setLevel(newLvl);
	    for (Handler h : handlers) {
	        if (h instanceof ConsoleHandler) {
	            h.setLevel(newLvl);
	            break;
	        }
	    }
	}
	
	public static GregorianCalendar parseDate(String string) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    String[] parts = string.split(" ");
	    if (parts.length != 2)
	    	throw new ParseException("There are no spaces in the date", 10);
	    
	    Date parsedDate = dateFormat.parse(string);
		GregorianCalendar calendar = new GregorianCalendar();
		
		calendar.clear(); // Hour, minute, second and millisecond are meaningless
		calendar.setTime(parsedDate);
		calendar.clear(Calendar.HOUR);
		calendar.clear(Calendar.HOUR_OF_DAY);
		
		calendar.setTimeZone(TimeZone.getTimeZone(parts[1]));
		return calendar;
	}
}
