package it.polito.dp2.FDS.tests;

import it.polito.dp2.FDS.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.*;

public abstract class TestsAbstract {
	private final static LoggerManager logger = new LoggerManager(TestsAbstract.class.getName());
	
	@BeforeClass
	public static void initializeTestCase() {
		Common.setDebugLevel(Common.minimumLoggingLevel);
	}
	
	@Before
	public void initializeTest() {
		System.clearProperty("it.polito.dp2.FDS.Random.seed");
		System.clearProperty("it.polito.dp2.FDS.Random.testcase");
		System.clearProperty("it.polito.dp2.FDS.FlightMonitorFactory");
		System.clearProperty(getFlightInfoPropertyKey());
		logger.info("*** Running test... ***");
	}
	
	protected File getTestFolder(String parentFolder, int testId) {
		logger.entering();
		Path testsFolder = FileSystems.getDefault().getPath(System.getProperty("java.io.tmpdir")).resolve("dp2-tests").resolve("ass0" + getAssignmentNumber()).resolve(parentFolder).resolve("test" + testId);
		for (int i = 1; ; ++i) {
			File testFolder = testsFolder.resolve(Integer.toString(i)).toFile();
			if (testFolder.mkdirs()) {
				logger.finest("Test " + testId + " is executed in folder " + testFolder.getAbsolutePath());
				logger.exiting();
				return testFolder; // example: "/tmp/dp2-tests/ass01/ghioluca/test1/1/"
			}
		}
	}
	
	protected Path getTestsFolder(String parentFolder, int testId) {
		logger.entering();
		Path reply = FileSystems.getDefault().getPath("./tests").resolve(parentFolder).resolve("test" + testId);
		logger.exiting();
		return reply;
	}
	
	protected File getTestFile(File testFolder, String fileName) {
		logger.entering();
		File reply = testFolder.toPath().resolve(fileName).toFile();
		logger.finest("File " + fileName + " in folder " + testFolder.getAbsolutePath() + " = " + reply.getAbsolutePath());
		logger.exiting();
		return reply;
	}
	
	protected void runSerializer(String outputFileName, int testCase, int seed) throws Exception {
		logger.entering();
		setTestCase(testCase);
		setSeed(seed);
		logger.finer("Running serializer with random generator and output file name = " + outputFileName + "...");
		runSerializer_private(outputFileName, "it.polito.dp2.FDS.Random.FlightMonitorFactoryImpl");
		logger.finer("Serializer run successfully");
		logger.exiting();
	}
	
	protected void runSerializer(String outputFileName, String simpleFlightMonitorFactoryClass) throws Exception {
		logger.entering();
		logger.finer("Running serializer with simple flight monitor and with output file name = " + outputFileName + "...");
		runSerializer_private(outputFileName, simpleFlightMonitorFactoryClass);
		logger.finer("Serializer run successfully");
		logger.exiting();
	}
	
	protected FlightMonitor runParser(File testFolder, String inputFileName) throws FlightMonitorException, FactoryConfigurationError, IOException {
		logger.entering();
		System.setProperty(getFlightInfoPropertyKey(), inputFileName);
		File schemaFile = getSchemaFile();
		Files.copy(schemaFile.toPath(), testFolder.toPath().resolve(schemaFile.getName()));
		
		logger.finer("Running parser with input file name = " + inputFileName + "...");
		FlightMonitor reply = runFlightMonitor(getParserClass());
		logger.finer("Parser run successfully");
		
		logger.exiting();
		return reply;
	}
	
	protected FlightMonitor runRandomGenerator(int testCase, int seed) throws FlightMonitorException {
		logger.entering();
		setTestCase(testCase);
		setSeed(seed);
		logger.finer("Running random generator...");
		FlightMonitor reply = runFlightMonitor("it.polito.dp2.FDS.Random.FlightMonitorFactoryImpl");
		logger.finer("Random generator run successfully");
		logger.exiting();
		return reply;
	}
	
	protected FlightMonitor runSimpleFlightMonitor(String simpleFlightMonitorFactoryClass) throws FlightMonitorException, FactoryConfigurationError {
		logger.entering();
		logger.finer("Running simple flight monitor...");
		FlightMonitor reply = runFlightMonitor(simpleFlightMonitorFactoryClass);
		logger.finer("Simple flight monitor run successfully");
		logger.exiting();
		return reply;
	}
	
	private FlightMonitor runFlightMonitor(String flightMonitorFactoryClass) throws FlightMonitorException, FactoryConfigurationError {
		logger.entering();
		System.setProperty("it.polito.dp2.FDS.FlightMonitorFactory", flightMonitorFactoryClass);
		logger.finest("Running flight monitor " + flightMonitorFactoryClass + "...");
		FlightMonitor reply = it.polito.dp2.FDS.FlightMonitorFactory.newInstance().newFlightMonitor();
		logger.finest("Flight monitor run successfully");
		logger.exiting();
		return reply;
	}
	
	private void runSerializer_private(String outputFileName, String flightMonitorFactoryClass) throws Exception {
		logger.entering();
		System.setProperty("it.polito.dp2.FDS.FlightMonitorFactory", flightMonitorFactoryClass);
		
		logger.finest("Running serializer with flight monitor " + flightMonitorFactoryClass + " and output file name = " + outputFileName + "...");
		runSerializerImpl(new String[] { outputFileName });
		logger.finest("Serializer run successfully");
		
		logger.exiting();
	}
	
	private void setTestCase(int testCase) {
		logger.entering();
		System.setProperty("it.polito.dp2.FDS.Random.testcase", Integer.toString(testCase));
		logger.exiting();
	}
	
	private void setSeed(int seed) {
		logger.entering();
		System.setProperty("it.polito.dp2.FDS.Random.seed", Integer.toString(seed));
		logger.exiting();
	}
	
	protected abstract int getAssignmentNumber();
	protected abstract void runSerializerImpl(String[] args) throws Exception;
	protected abstract String getParserClass();
	protected abstract String getFlightInfoPropertyKey();
	protected abstract File getSchemaFile();
}