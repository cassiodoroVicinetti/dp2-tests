package it.polito.dp2.FDS.tests;

import java.util.logging.Level;
import java.util.logging.Logger;

public class LoggerManager {
	private final Logger logger;
	private final int CLIENT_CODE_STACK_INDEX;
	
	public LoggerManager(String className) {
		logger = Logger.getLogger(className);
		
		// Finds out the index of "this code" in the returned stack trace - funny but it differs in JDK 1.5 and 1.6
        int i = 0;
        for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
            i++;
            if (ste.getClassName().equals(className)) {
                break;
            }
        }
        CLIENT_CODE_STACK_INDEX = i;
	}
	
	public void entering() {
		logger.entering(logger.getName(), getMethodName(0));
	}
	
	public void exiting() {
		logger.exiting(logger.getName(), getMethodName(0));
	}
	
	public void throwing(Throwable e) {
		logger.throwing(logger.getName(), getMethodName(0), e);
	}
	
	public void config(String msg) {
		logger.logp(Level.CONFIG, logger.getName(), getMethodName(0), msg);
	}
	
	public void fine(String msg) {
		logger.logp(Level.FINE, logger.getName(), getMethodName(0), msg);
	}
	
	public void finer(String msg) {
		logger.logp(Level.FINER, logger.getName(), getMethodName(0), msg);
	}
	
	public void finest(String msg) {
		logger.logp(Level.FINEST, logger.getName(), getMethodName(0), msg);
	}
	
	public void info(String msg) {
		logger.logp(Level.INFO, logger.getName(), getMethodName(0), msg);
	}
	
	public void severe(String msg) {
		logger.logp(Level.SEVERE, logger.getName(), getMethodName(0), msg);
	}
	
	public void warning(String msg) {
		logger.logp(Level.WARNING, logger.getName(), getMethodName(0), msg);
	}
	
    private String getMethodName(int depth) {
    	// http://stackoverflow.com/a/8592871/6309
        return Thread.currentThread().getStackTrace()[CLIENT_CODE_STACK_INDEX + depth].getMethodName();
    }
}