package it.polito.dp2.FDS.lab1.tests.ghioluca.test14_limit;

import java.text.ParseException;
import java.util.*;

import it.polito.dp2.FDS.*;
import it.polito.dp2.FDS.tests.*;

public class SimpleFlightMonitor extends it.polito.dp2.FDS.tests.SimpleFlightMonitor {
	public SimpleFlightMonitor() throws FlightMonitorException {
		super();
	}

	@Override
	public void fill() throws FlightMonitorException {
		HashSet<String> seats = new HashSet<String>();
		seats.add("1A");
		Aircraft aircraft = new Aircraft("Boeing747", seats);
		addAircraft(aircraft);
		
		SimpleFlightReader flight = new SimpleFlightReader("XYZ", new Time(12, 30), "ABC", "AB123");
		addFlight(flight);
		
		SimpleFlightInstanceReader flightInstance;
		try {
			flightInstance = new SimpleFlightInstanceReader(aircraft, it.polito.dp2.FDS.tests.Common.parseDate("2014-11-08 GMT+01:00"), 10, "Gate01", flight, FlightInstanceStatus.ARRIVED);
		} catch (ParseException e) {
			throw new FlightMonitorException(e);
		}
		flight.addFlightInstance(flightInstance);
		
		SimpleFlightInstanceReader flightInstance2;
		try {
			flightInstance2 = new SimpleFlightInstanceReader(aircraft, it.polito.dp2.FDS.tests.Common.parseDate("2014-11-08 Europe/Rome"), 11, "Gate02", flight, FlightInstanceStatus.BOARDING);
		} catch (ParseException e) {
			throw new FlightMonitorException(e);
		}
		flight.addFlightInstance(flightInstance2);
		
		flightInstance.addPassenger(new SimplePassengerReader(true, flightInstance, "Pinco Pallino", "1A"));
	}
}